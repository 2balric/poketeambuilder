const urlBase = 'https://pokeapi.co/api/v2/';
export const urlPokemon = (pokemon : string) => `${urlBase}pokemon/${pokemon}`;
export const urlPokemonList = (begin : number, limit : number) => `${urlBase}pokemon?limit=${limit}&offset=${begin}`;
export const urlPokemonListCache = () => '/api/pokemon.json';