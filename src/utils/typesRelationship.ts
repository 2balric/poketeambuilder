import Pokemon from '../entities/Pokemon'
import Type, { getTypeByName, TypeRelation } from './../entities/Type'
//https://as01.epimg.net/meristation/imagenes/2019/12/01/guia_pagina/1575197597_259376_1575367068_sumario_normal.jpg

/*
[Type.Normal]: 1,
[Type.Fire]: 1,
[Type.Water]: 1,
[Type.Grass]: 1,
[Type.Electric]: 1,
[Type.Ice]: 1,
[Type.Fighting]: 1,
[Type.Poison]: 1,
[Type.Ground]: 1,
[Type.Flying]: 1,
[Type.Psychic]: 1,
[Type.Bug]: 1,
[Type.Rock]: 1,
[Type.Ghost]: 1,
[Type.Dark]: 1,
[Type.Dragon]: 1,
[Type.Steel]: 1,
[Type.Fairy]: 1
*/
const typesRelationship = {
    [Type.Normal]: {
        [Type.Normal]: 1,
        [Type.Fire]: 1,
        [Type.Water]: 1,
        [Type.Grass]: 1,
        [Type.Electric]: 1,
        [Type.Ice]: 1,
        [Type.Fighting]: 1,
        [Type.Poison]: 1,
        [Type.Ground]: 1,
        [Type.Flying]: 1,
        [Type.Psychic]: 1,
        [Type.Bug]: 1,
        [Type.Rock]: .5,
        [Type.Ghost]: 0,
        [Type.Dark]: 1,
        [Type.Dragon]: 1,
        [Type.Steel]: .5,
        [Type.Fairy]: 1,
    },
    [Type.Fighting]: {
        [Type.Normal]: 2,
        [Type.Fire]: 1,
        [Type.Water]: 1,
        [Type.Grass]: 1,
        [Type.Electric]: 1,
        [Type.Ice]: 2,
        [Type.Fighting]: 1,
        [Type.Poison]: .5,
        [Type.Ground]: 1,
        [Type.Flying]: .5,
        [Type.Psychic]: .5,
        [Type.Bug]: .5,
        [Type.Rock]: 2,
        [Type.Ghost]: 0,
        [Type.Dark]: 2,
        [Type.Dragon]: 1,
        [Type.Steel]: 2,
        [Type.Fairy]: .5
    },
    [Type.Flying]: {
        [Type.Normal]: 1,
        [Type.Fire]: 1,
        [Type.Water]: 1,
        [Type.Grass]: 2,
        [Type.Electric]: .5,
        [Type.Ice]: 1,
        [Type.Fighting]: 2,
        [Type.Poison]: 1,
        [Type.Ground]: 1,
        [Type.Flying]: 1,
        [Type.Psychic]: 1,
        [Type.Bug]: 2,
        [Type.Rock]: .5,
        [Type.Ghost]: 1,
        [Type.Dark]: 1,
        [Type.Dragon]: 1,
        [Type.Steel]: .5,
        [Type.Fairy]: 1,
    },
    [Type.Poison]: {
        [Type.Normal]: 1,
        [Type.Fire]: 1,
        [Type.Water]: 1,
        [Type.Grass]: 2,
        [Type.Electric]: 1,
        [Type.Ice]: 1,
        [Type.Fighting]: 1,
        [Type.Poison]: .5,
        [Type.Ground]: .5,
        [Type.Flying]: 1,
        [Type.Psychic]: 1,
        [Type.Bug]: 1,
        [Type.Rock]: .5,
        [Type.Ghost]: .5,
        [Type.Dark]: 1,
        [Type.Dragon]: 1,
        [Type.Steel]: 0,
        [Type.Fairy]: 2
    },
    [Type.Ground]: {
        [Type.Normal]: 1,
        [Type.Fire]: 2,
        [Type.Water]: 1,
        [Type.Grass]: .5,
        [Type.Electric]: 2,
        [Type.Ice]: 1,
        [Type.Fighting]: 1,
        [Type.Poison]: 2,
        [Type.Ground]: 1,
        [Type.Flying]: 0,
        [Type.Psychic]: 1,
        [Type.Bug]: .5,
        [Type.Rock]: 2,
        [Type.Ghost]: 1,
        [Type.Dark]: 1,
        [Type.Dragon]: 1,
        [Type.Steel]: 2,
        [Type.Fairy]: 1,
    },
    [Type.Rock]: {
        [Type.Normal]: 1,
        [Type.Fire]: 2,
        [Type.Water]: 1,
        [Type.Grass]: 1,
        [Type.Electric]: 1,
        [Type.Ice]: 2,
        [Type.Fighting]: .5,
        [Type.Poison]: 1,
        [Type.Ground]: .5,
        [Type.Flying]: 2,
        [Type.Psychic]: 1,
        [Type.Bug]: 2,
        [Type.Rock]: 1,
        [Type.Ghost]: 1,
        [Type.Dark]: 1,
        [Type.Dragon]: 1,
        [Type.Steel]: .5,
        [Type.Fairy]: 1
    },
    [Type.Bug]: {
        [Type.Normal]: 1,
        [Type.Fire]: .5,
        [Type.Water]: 1,
        [Type.Grass]: 2,
        [Type.Electric]: 1,
        [Type.Ice]: 1,
        [Type.Fighting]: .5,
        [Type.Poison]: .5,
        [Type.Ground]: 1,
        [Type.Flying]: .5,
        [Type.Psychic]: 2,
        [Type.Bug]: 1,
        [Type.Rock]: 1,
        [Type.Ghost]: .5,
        [Type.Dark]: 2,
        [Type.Dragon]: 1,
        [Type.Steel]: .5,
        [Type.Fairy]: .5
    },
    [Type.Ghost]: {
        [Type.Normal]: 0,
        [Type.Fire]: 1,
        [Type.Water]: 1,
        [Type.Grass]: 1,
        [Type.Electric]: 1,
        [Type.Ice]: 1,
        [Type.Fighting]: 1,
        [Type.Poison]: 1,
        [Type.Ground]: 1,
        [Type.Flying]: 1,
        [Type.Psychic]: 2,
        [Type.Bug]: 1,
        [Type.Rock]: 1,
        [Type.Ghost]: 2,
        [Type.Dark]: .5,
        [Type.Dragon]: 1,
        [Type.Steel]: 1,
        [Type.Fairy]: 1
    },
    [Type.Steel]: {
        [Type.Normal]: 1,
        [Type.Fire]: .5,
        [Type.Water]: .5,
        [Type.Grass]: 1,
        [Type.Electric]: .5,
        [Type.Ice]: 2,
        [Type.Fighting]: 1,
        [Type.Poison]: 1,
        [Type.Ground]: 1,
        [Type.Flying]: 1,
        [Type.Psychic]: 1,
        [Type.Bug]: 1,
        [Type.Rock]: 2,
        [Type.Ghost]: 1,
        [Type.Dark]: 1,
        [Type.Dragon]: 1,
        [Type.Steel]: .5,
        [Type.Fairy]: 2
    },
    [Type.Fire]: {
        [Type.Normal]: 1,
        [Type.Fire]: .5,
        [Type.Water]: .5,
        [Type.Grass]: 2,
        [Type.Electric]: 1,
        [Type.Ice]: 2,
        [Type.Fighting]: 1,
        [Type.Poison]: 1,
        [Type.Ground]: 1,
        [Type.Flying]: 1,
        [Type.Psychic]: 1,
        [Type.Bug]: 2,
        [Type.Rock]: .5,
        [Type.Ghost]: 1,
        [Type.Dark]: 1,
        [Type.Dragon]: .5,
        [Type.Steel]: 2,
        [Type.Fairy]: 1,
    },
    [Type.Water]: {
        [Type.Normal]: 1,
        [Type.Fire]: 2,
        [Type.Water]: .5,
        [Type.Grass]: .5,
        [Type.Electric]: 1,
        [Type.Ice]: 1,
        [Type.Fighting]: 1,
        [Type.Poison]: 1,
        [Type.Ground]: 2,
        [Type.Flying]: 1,
        [Type.Psychic]: 1,
        [Type.Bug]: 1,
        [Type.Rock]: 2,
        [Type.Ghost]: 1,
        [Type.Dark]: 1,
        [Type.Dragon]: .5,
        [Type.Steel]: 1,
        [Type.Fairy]: 1
    },
    [Type.Grass]: {
        [Type.Normal]: 1,
        [Type.Fire]: .5,
        [Type.Water]: 2,
        [Type.Grass]: .5,
        [Type.Electric]: 1,
        [Type.Ice]: 1,
        [Type.Fighting]: 1,
        [Type.Poison]: .5,
        [Type.Ground]: 2,
        [Type.Flying]: .5,
        [Type.Psychic]: 1,
        [Type.Bug]: .5,
        [Type.Rock]: 2,
        [Type.Ghost]: 1,
        [Type.Dark]: 1,
        [Type.Dragon]: .5,
        [Type.Steel]: .5,
        [Type.Fairy]: 1
    },
    [Type.Electric]: {
        [Type.Normal]: 1,
        [Type.Fire]: 1,
        [Type.Water]: 2,
        [Type.Grass]: .5,
        [Type.Electric]: .5,
        [Type.Ice]: 1,
        [Type.Fighting]: 1,
        [Type.Poison]: 1,
        [Type.Ground]: 0,
        [Type.Flying]: 2,
        [Type.Psychic]: 1,
        [Type.Bug]: 1,
        [Type.Rock]: 1,
        [Type.Ghost]: 1,
        [Type.Dark]: 1,
        [Type.Dragon]: .5,
        [Type.Steel]: 1,
        [Type.Fairy]: 1
    },
    [Type.Psychic]: {
        [Type.Normal]: 1,
        [Type.Fire]: 1,
        [Type.Water]: 1,
        [Type.Grass]: 1,
        [Type.Electric]: 1,
        [Type.Ice]: 1,
        [Type.Fighting]: 2,
        [Type.Poison]: 2,
        [Type.Ground]: 1,
        [Type.Flying]: 1,
        [Type.Psychic]: .5,
        [Type.Bug]: 1,
        [Type.Rock]: 1,
        [Type.Ghost]: 1,
        [Type.Dark]: 0,
        [Type.Dragon]: 1,
        [Type.Steel]: .5,
        [Type.Fairy]: 1
    },
    [Type.Ice]: {
        [Type.Normal]: 1,
        [Type.Fire]: .5,
        [Type.Water]: .5,
        [Type.Grass]: 2,
        [Type.Electric]: 1,
        [Type.Ice]: .5,
        [Type.Fighting]: 1,
        [Type.Poison]: 1,
        [Type.Ground]: 2,
        [Type.Flying]: 2,
        [Type.Psychic]: 1,
        [Type.Bug]: 1,
        [Type.Rock]: 1,
        [Type.Ghost]: 1,
        [Type.Dark]: 1,
        [Type.Dragon]: 2,
        [Type.Steel]: .5,
        [Type.Fairy]: 1
    },
    [Type.Dragon]: {
        [Type.Normal]: 1,
        [Type.Fire]: 1,
        [Type.Water]: 1,
        [Type.Grass]: 1,
        [Type.Electric]: 1,
        [Type.Ice]: 1,
        [Type.Fighting]: 1,
        [Type.Poison]: 1,
        [Type.Ground]: 1,
        [Type.Flying]: 1,
        [Type.Psychic]: 1,
        [Type.Bug]: 1,
        [Type.Rock]: 1,
        [Type.Ghost]: 1,
        [Type.Dark]: 1,
        [Type.Dragon]: 2,
        [Type.Steel]: .5,
        [Type.Fairy]: 0
    },
    [Type.Dark]: {
        [Type.Normal]: 1,
        [Type.Fire]: 1,
        [Type.Water]: 1,
        [Type.Grass]: 1,
        [Type.Electric]: 1,
        [Type.Ice]: 1,
        [Type.Fighting]: .5,
        [Type.Poison]: 1,
        [Type.Ground]: 1,
        [Type.Flying]: 1,
        [Type.Psychic]: 2,
        [Type.Bug]: 1,
        [Type.Rock]: 1,
        [Type.Ghost]: 2,
        [Type.Dark]: .5,
        [Type.Dragon]: 1,
        [Type.Steel]: 1,
        [Type.Fairy]: .5
    },
    [Type.Fairy]: {
        [Type.Normal]: 1,
        [Type.Fire]: .5,
        [Type.Water]: 1,
        [Type.Grass]: 1,
        [Type.Electric]: 1,
        [Type.Ice]: 1,
        [Type.Fighting]: 2,
        [Type.Poison]: .5,
        [Type.Ground]: 1,
        [Type.Flying]: 1,
        [Type.Psychic]: 1,
        [Type.Bug]: 1,
        [Type.Rock]: 1,
        [Type.Ghost]: 1,
        [Type.Dark]: 2,
        [Type.Dragon]: 2,
        [Type.Steel]: .5,
        [Type.Fairy]: 1
    }
}

const getTypeRelations = (types : Type[]) => {
    const typeRelations : TypeRelation[] = []
    for(const [row, col] of Object.entries(typesRelationship)){
        const typeRel = getTypeByName(row)
        let factor = 1
        for(const type of types){
            const enumType = getTypeByName(type)
            factor*=col[enumType]
        }
        typeRelations.push({type: typeRel, factor})
    }
    return typeRelations
}
const getWeaknesses = (types : Type[]) => getTypeRelations(types).filter( tr => tr.factor > 1)

const getResistances = (types : Type[]) => getTypeRelations(types).filter( tr => tr.factor < 1)

const getTeamResistancesWeaknesses = (team : Pokemon[]) => {
    const resistances : TypeRelation[] = []
    const weaknesses : TypeRelation[] = []
    for(const pokemon of team){
        const relations = getTypeRelations(pokemon.types)
        for( const relation of relations){
            const existingResistance = resistances.find( r => r.type === relation.type)
            const existingWeakness = weaknesses.find( r => r.type === relation.type)
            if(relation.factor < 1){
                if(existingResistance){
                    existingResistance.factor++
                }else{
                    resistances.push({...relation, factor: 1})
                }
            }else if ( relation.factor > 1){
                if(existingWeakness){
                    existingWeakness.factor++
                }else{
                    weaknesses.push({...relation, factor: 1})
                }
            }
        }
    }
    return { resistances, weaknesses}
} 

export default typesRelationship
export { getWeaknesses, getResistances, getTypeRelations, getTeamResistancesWeaknesses }