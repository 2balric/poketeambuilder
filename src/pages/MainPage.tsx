import React from 'react'
import Grid from '@material-ui/core/Grid'

import Team from '../components/Team/Team'

import { useTeamStateContext } from './../TeamContext'
import TypeRelationList from '../components/TypeRelationList'

import { getTeamResistancesWeaknesses } from '../utils/typesRelationship'

const MainPage = () => {
    const state = useTeamStateContext()
    const { team } = state
    const { resistances, weaknesses } = getTeamResistancesWeaknesses(team)
    return (
        <Grid container justify="center" direction="row" className="fullpage">
                <Grid container item justify="space-between" direction="row" md={4} sm={12}>
                    <Team team={team}/>
                </Grid>
                <Grid container item justify="space-between" direction="row" md={8} className="tables">
                    <Grid container item justify="center" direction="row" md={6}>
                        <TypeRelationList title="Resitencias" typeRelations={resistances} />
                    </Grid>
                    <Grid container item justify="center" direction="row" md={6}>
                        <TypeRelationList title="Debilidades" typeRelations={weaknesses} />
                    </Grid>
                </Grid>
        </Grid>
    )
}

export default MainPage
