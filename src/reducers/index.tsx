import Store from './../entities/Store'
import { ADD_POKEMON, REMOVE_POKEMON } from './../actions/constants'

const baseReducer : any = {
    [ADD_POKEMON]: (state : Store, action : any) => {
        const newPokemon = action.payload
        const addedTeam = [...state.team, newPokemon]
        return {...state, team: addedTeam}
    },
    [REMOVE_POKEMON]: (state : Store, action : any) => {
        const idPokemon = action.payload
        const deletedTeam = state.team.filter(p => p.id !== idPokemon)
        return {...state, team: deletedTeam}
    }
}

const reducer = (state : Store, action : any) => baseReducer[action.type] && baseReducer[action.type](state, action)
export default reducer