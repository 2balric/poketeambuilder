declare global{
    interface String{
        toCamelCase(): string;
    }
}

String.prototype.toCamelCase = function (): string {
    if(!this) return this
    return this[0].toUpperCase() + this.substr(1).toLowerCase();
  }
  export{}