import React from 'react'
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import Pokemon from '../../entities/Pokemon'
import Type from '../../entities/Type'

import { useTeamDispatchContext } from './../../TeamContext'
import { removePokemonFromTeam } from './../../actions/removePokemonFromTeam'
import TypeLabel from '../TypeLabel';

import './PokemonInfo.scss';

type Prop = {
    pokemon: Pokemon
}
const renderTypes = (types: Type[]) => types.map( t => <TypeLabel type={t} key={t} />)
const PokemonInfo : React.FC<Prop> = ({pokemon}) => {
    const dispatch = useTeamDispatchContext()
    const { id, name, types } = pokemon
    return (
        <div className="poke-capsule">
            <Grid container justify="center">
                <Grid item lg={3} sm={12}>
                    <img src={`/images/pokemon/icons/${id}.png`} alt={name}/>
                </Grid>
                <Grid container item direction="row" lg={7} justify="flex-start" className="flex-center-small">
                    <Typography variant="h5" align="center" className="responsive-title">{name.toCamelCase()}</Typography>
                    <Grid container justify="flex-start" className="flex-center-small">
                        {renderTypes(types)}
                    </Grid>
                </Grid>
                <Grid container item lg={2} alignContent="center" className="flex-center-small">
                    {
                        id ? <Button size="small" color="secondary" onClick={() => dispatch(removePokemonFromTeam(id))}>
                            ❌
                        </Button> : ''
                    }
                </Grid>
            </Grid>
        </div>
    )
}

export default PokemonInfo