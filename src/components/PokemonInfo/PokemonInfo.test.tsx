import React from 'react'
import { render } from '@testing-library/react'
import PokemonInfo from '.'
import { team1 } from './../../tests/team'
import Type, { getTypeByName } from './../../entities/Type'
import './../../typings.d'

test("Bulbasaur render has correct data", async () => {
    const { findByText } = render(<PokemonInfo pokemon={team1[0]}/>)
    
    const bulbasaur = await findByText(/Bulbasaur/)
    const grass = await findByText(/Grass/)
    const poison = await findByText(/Poison/)

    const grassText = getTypeByName(grass.textContent||'')
    const poisonText = getTypeByName(poison.textContent||'')
    
    expect(bulbasaur).toHaveTextContent("Bulbasaur");
    expect(grass).toHaveTextContent("Grass");
    expect(grassText).toBe(Type.Grass)
    expect(poison).toHaveTextContent("Poison");
    expect(poisonText).toBe(Type.Poison)
    
})
