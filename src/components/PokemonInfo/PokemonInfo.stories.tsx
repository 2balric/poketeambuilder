import React from 'react'
import PokemonInfo from '.'
import { team1 } from '../../tests/team'

const story = {
    title: "PokemonInfo",
    component: PokemonInfo
}
export default story

export const BulbasaurInfoExample = () => <PokemonInfo pokemon={team1[0]}/>
export const TrevenantInfoExample = () => <PokemonInfo pokemon={team1[1]}/>