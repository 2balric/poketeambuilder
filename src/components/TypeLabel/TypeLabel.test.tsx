import React from 'react'
import { render } from '@testing-library/react'
import TypeLabel from '.'
import Type from '../../entities/Type'

test("TypeLabel fairy renders", async () => {
    const { findByText } = render(<TypeLabel type={Type.Fairy}/>)  
    const fairy = await findByText(/Fairy/)
    expect(fairy).toHaveTextContent("Fairy")
    expect(fairy).toHaveClass("pokemon-type")
    expect(fairy).toHaveClass("type-fairy")

    //No se puede testear css
    //"fairy": #F0B6BC
})

