import React from 'react'

import Type from '../../entities/Type'

import './TypeLabel.scss'

type Prop = {
    type: Type
}
const TypeLabel : React.FC<Prop> = ({type}) => {
    return (
        <span className={`pokemon-type type-${type.toLowerCase()}`}>
            {type}
        </span>
    )
}

export default TypeLabel
