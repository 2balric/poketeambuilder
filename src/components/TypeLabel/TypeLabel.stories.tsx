import React from 'react'
import TypeLabel from '.'
import Type from '../../entities/Type'

const story = {
    title: "TypeLabel",
    component: TypeLabel
}
export default story

const TypeLabelTemplate = (type : Type) => () => <TypeLabel type={type} />
const TypeLabelFairy = TypeLabelTemplate(Type.Fairy)
const TypeLabelNormal = TypeLabelTemplate(Type.Normal)
const TypeLabelDragon = TypeLabelTemplate(Type.Dragon)
const TypeLabelFire = TypeLabelTemplate(Type.Fire)
const TypeLabelGrass = TypeLabelTemplate(Type.Grass)
const TypeLabelWater = TypeLabelTemplate(Type.Water)

export {
    TypeLabelFairy,
    TypeLabelNormal,
    TypeLabelDragon,
    TypeLabelFire,
    TypeLabelGrass,
    TypeLabelWater
}