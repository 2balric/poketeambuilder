import React from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography';


import PokemonInfo from '../PokemonInfo'
import Pokemon, { missingno } from './../../entities/Pokemon'

import Store from '../../entities/Store'
import PokemonSelect from '../PokemonSelect';

import './Team.scss';

const MaxPokemonInTeam : number = 6

const renderPokemonInfo = (team : Pokemon[]) => {
    const pokemons = team.map( (poke, index) => <PokemonInfo key={index} pokemon={poke} />)
    for(let i = pokemons.length; i < MaxPokemonInTeam; i++ )
        pokemons.push(<PokemonInfo key={i} pokemon={missingno} />)
    return pokemons
}

const Team : React.FC<Store> = ({team}) => {
    return (
        <div className="poketeam">
            <Typography variant="h2" className="title">
                <img width="50px" alt="pokeball" src="/images/pokeball.png"/>
                Pokemon
            </Typography>
            <div className="team-container">
                {renderPokemonInfo(team)}
            </div>
            {
                team.length < MaxPokemonInTeam && <PokemonSelect />
            }
        </div>
    )
}

export default Team
