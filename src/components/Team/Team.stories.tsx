import React from 'react'
import Team from '.'
import { team1 } from '../../tests/team'

const story = {
    title: "Team",
    component: Team
}
export default story

export const TeamExample = () => <Team team={team1}/>