import React from 'react'
import { render } from '@testing-library/react'
import Team from '.'
import { team1 } from './../../tests/team'
import './../../typings.d'
test("CityList renders", async () => {
    const { findByText } = render(<Team team={team1}/>)
    
    const bulbasaur = await findByText(/Bulbasaur/)
    const trevenant = await findByText(/Trevenant/)
    
    expect(bulbasaur).toHaveTextContent("Bulbasaur");
    expect(trevenant).toHaveTextContent("Trevenant");    
})
