import React from 'react'

import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';

import lookForPokemon from './../../actions/lookForPokemon'
import getPokemonList from './../../actions/getPokemonList'
import Pokemon from '../../entities/Pokemon';
import { useTeamDispatchContext } from './../../TeamContext'
import { addPokemonToTeam } from './../../actions/addPokemonToTeam'
import { removePokemonFromTeam } from './../../actions/removePokemonFromTeam'

const selectPokemonHandler = (dispatch : any) => (event: React.ChangeEvent<{}>, value: Pokemon | null) => {
    if(!value)
    return
  const pokemonDefault = {
    id: 0,
    name: value.name,
    types: []
  };
  dispatch(addPokemonToTeam(pokemonDefault))
  lookForPokemon(value.name).then( pokemon => {
    if(pokemon){
      dispatch(removePokemonFromTeam(0)) 
      dispatch(addPokemonToTeam(pokemon))
    }
  }) 
}
const PokemonSelect = () => {
  const dispatch = useTeamDispatchContext()
  const [open, setOpen] = React.useState(false);
  const [options, setOptions] = React.useState<Pokemon[]>([]);
  const loading = open && options.length === 0;

  React.useEffect(() => {
    let active = true;

    if (!loading) {
      return undefined;
    }

    (async () => {
      const pokemonList = await getPokemonList(0, 200)

      if (active) {
        setOptions(pokemonList);
      }
    })();

    return () => {
      active = false;
    };
  }, [loading]);
    return (
      <Autocomplete
        id="asynchronous-demo"
        style={{ width: 300 }}
        open={open}
        onOpen={() => {
          setOpen(true);
        }}
        onClose={() => {
          setOpen(false);
        }}
        onChange={selectPokemonHandler(dispatch)}
        getOptionSelected={(option, value) => option.name === value.name}
        getOptionLabel={(option) => option.name}
        options={options}
        loading={loading}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Pokemon"
            color="secondary"
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <React.Fragment>
                  {loading ? <CircularProgress color="inherit" size={20} /> : null}
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
          />
        )}
    />
    )
}

export default PokemonSelect
