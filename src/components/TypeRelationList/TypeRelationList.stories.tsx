import React from 'react'
import TypeRelationList from '.'
import Type, { TypeRelation } from '../../entities/Type'

const story = {
    title: "TypeRelationList",
    component: TypeRelationList
}
export default story

const typeRelationsExample : TypeRelation[] = [
    {factor: 2, type: Type.Dark},
    {factor: 0, type: Type.Ghost}
]
export const TypeRelationListExample = () => <TypeRelationList title="Resistencias" typeRelations={typeRelationsExample}/>