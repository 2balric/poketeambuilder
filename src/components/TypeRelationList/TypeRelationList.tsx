import React from 'react'

import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/List'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'

import { TypeRelation } from '../../entities/Type'
import TypeLabel from '../TypeLabel'

import './TypeRelationList.scss';

type Prop = {
    title: string,
    typeRelations: TypeRelation[]
}

const TypeRelationList : React.FC<Prop> = ({title, typeRelations}) => {
    return (
        <Grid container direction="column" justify="flex-start" alignContent="center">
            <h2 className="title">{title}</h2>
            <List>
                { typeRelations
                    .sort( (a : TypeRelation, b: TypeRelation) => b.factor - a.factor )
                    .map( (tr : TypeRelation, index: number) => <ListItem key={index}><b>{tr.factor}x</b> <TypeLabel type={tr.type} /></ListItem>)
                }
            </List>
            <Typography variant="h4" className="total">{typeRelations.length} {title.toLocaleLowerCase()} en total</Typography>
        </Grid>
    )
}

export default TypeRelationList