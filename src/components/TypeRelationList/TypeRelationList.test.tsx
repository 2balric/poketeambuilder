import React from 'react'
import { render } from '@testing-library/react'
import TypeRelationList from '.'
import Type, { TypeRelation } from '../../entities/Type'

const typeRelationsExample : TypeRelation[] = [
    {factor: 2, type: Type.Dark},
    {factor: 0, type: Type.Ghost}
]
test("TypeRelationList renders", async () => {
    const { findByText } = render(<TypeRelationList title="Resistencias" typeRelations={typeRelationsExample}/>)
    
    const title = await findByText(/Resistencias/)
    
    expect(title).toHaveTextContent("Resistencias");
})