import Type from './Type'

export default interface Pokemon{
    id: number,
    name: string,
    types: Type[]
}
export const missingno : Pokemon = {
    id: 0,
    name: '',
    types: []
};