import enumKeys from './../utils/enumKeys'

enum Type {
    Normal = 'Normal',
    Fire = 'Fire',
    Water = 'Water',
    Grass = 'Grass',
    Electric = 'Electric',
    Ice = 'Ice',
    Fighting = 'Fighting',
    Poison = 'Poison',
    Ground = 'Ground',
    Flying = 'Flying',
    Psychic = 'Psychic',
    Bug = 'Bug',
    Rock = 'Rock',
    Ghost = 'Ghost',
    Dark = 'Dark',
    Dragon = 'Dragon',
    Steel = 'Steel',
    Fairy = 'Fairy',
}


export interface TypeRelation{
    type: Type,
    factor: number
}

export const getTypeByName = (type : string) => {
    if(!type) return Type.Normal
    for (const value of enumKeys(Type)) {
        if(value.toLowerCase() === type.toLowerCase()) return Type[value]
    }
    return Type.Normal
}

export default Type