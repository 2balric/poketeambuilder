import React from 'react';
import './App.css';
import MainPage from './pages/MainPage'
import TeamContext from './TeamContext'

function App() {
  return (
    <TeamContext>
      <MainPage/>
    </TeamContext>
  );
}

export default App;
