import React, { useCallback, useReducer, useContext } from 'react'
import Store from './entities/Store'
import globalReducer from './reducers'

const initialValue : Store = {
    team: []
}

const TeamStateContext = React.createContext<Store>(initialValue)
const TeamDispatchContext = React.createContext<React.Dispatch<any>|any>({})

type Prop = {
    children: React.ReactNode
}
const TeamContext : React.FC<Prop> = ({children}) => {
    const reducer = useCallback(globalReducer,[])
    const [state, dispatch] = useReducer(reducer, initialValue)
    return (
        <TeamDispatchContext.Provider value={dispatch}>
            <TeamStateContext.Provider value={state}>
                {children}
            </TeamStateContext.Provider>
        </TeamDispatchContext.Provider>
    )
}

const useTeamDispatchContext = () => {
    const dispatch = useContext(TeamDispatchContext)
    if(!dispatch)
        throw Error("Must set dispatch provider")
    return dispatch
}
const useTeamStateContext = () => {
    const state = useContext(TeamStateContext)
    if(!state)
        throw Error("Must set state provider")
    return state
}

export default TeamContext
export {
    useTeamDispatchContext,
    useTeamStateContext
}