import Pokemon from "../entities/Pokemon";
import Type from "../entities/Type";

export const team1 : Pokemon[] = [
    {
        id: 1,
        name: "Bulbasaur",
        types: [ Type.Grass, Type.Poison ]
    },
    {
        id: 700,
        name: "Trevenant",
        types: [ Type.Grass, Type.Ghost ]
    }
]

