import Pokemon from '../entities/Pokemon'
import { getTypeByName } from '../entities/Type'
import { apiGet } from './../api'
import { urlPokemon } from './../api/urls'

const lookForPokemon = async (pokemon : string) : Promise<Pokemon> => {
    const response = await apiGet(urlPokemon(pokemon))
    const { id, name, types } = response
    const newTypes = types.map( (t : any) => getTypeByName(t.type.name))
    return {
        id, name, types: newTypes
    }
}
export default lookForPokemon