import Pokemon from '../entities/Pokemon'
import { apiGet } from '../api/index.js'
import { urlPokemonListCache } from '../api/urls'

//export const getPokemonListRaw = async (begin : number, limit : number) => await apiGet(urlPokemonList(begin, limit))
export const getPokemonListRaw = async (begin : number, limit : number) => await apiGet(urlPokemonListCache())

const getPokemonList = async (begin : number, limit : number) : Promise<Pokemon[]> => {
    const response = await getPokemonListRaw(begin, limit)
    return response.map( (r : any, index: number) => ({ id: index, name: r.name, types: [] } as Pokemon))
}

export default getPokemonList