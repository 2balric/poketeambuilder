import { REMOVE_POKEMON } from './constants'

export const removePokemonFromTeam = (pokemon : number) => {
    return ({type: REMOVE_POKEMON, payload: pokemon})
}