const ADD_POKEMON = 'ADD_POKEMON'
const REMOVE_POKEMON = 'REMOVE_POKEMON'
export {
    ADD_POKEMON,
    REMOVE_POKEMON
}