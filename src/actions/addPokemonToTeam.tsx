import { ADD_POKEMON } from './constants'
import Pokemon from '../entities/Pokemon'

export const addPokemonToTeam = (pokemon : Pokemon) => {
    return ({type: ADD_POKEMON, payload: pokemon})
}